package ru.nsu.asidorov;

import java.util.Map;
import java.util.Stack;

public interface Command {
    void getName();

    void doCommand(Map<String, Double> context, Stack<Double> stack, String[] param);//TODO: набор аргуметво переменное как в printf
}
