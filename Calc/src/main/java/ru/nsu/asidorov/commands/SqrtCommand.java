package ru.nsu.asidorov.commands;

import ru.nsu.asidorov.Command;

import java.util.EmptyStackException;
import java.util.Map;
import java.util.Stack;

public class SqrtCommand implements Command {
    @Override
    public void getName() {
        System.out.println(this.getClass());
    }

    @Override
    public void doCommand(Map<String, Double> context, Stack<Double> stack, String[] param) throws EmptyStackException, IllegalArgumentException {

        if (stack.size() < 1) {
            throw new EmptyStackException();
        }

        double d = stack.pop();
        if (d < 0.0) throw new IllegalArgumentException("negative sqtr arg");
        stack.push(Math.sqrt(d));
    }
}
