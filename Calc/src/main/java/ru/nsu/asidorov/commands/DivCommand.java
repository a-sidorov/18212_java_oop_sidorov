package ru.nsu.asidorov.commands;

import ru.nsu.asidorov.Command;

import java.util.EmptyStackException;
import java.util.Map;
import java.util.Stack;

public class DivCommand implements Command {


    @Override
    public void getName() {
        System.out.println(this.getClass());
    }

    @Override
    public void doCommand(Map<String, Double> context, Stack<Double> stack, String[] param) throws EmptyStackException, IllegalArgumentException {

        if (stack.size() < 2) {
            throw new EmptyStackException();
        }

        double a = stack.pop();
        double b = stack.pop();
        if (b == 0) throw new IllegalArgumentException("Division by zero");
        stack.push(a / b);
    }
}
