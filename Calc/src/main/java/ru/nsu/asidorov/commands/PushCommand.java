package ru.nsu.asidorov.commands;

import ru.nsu.asidorov.Command;

import java.util.Map;
import java.util.Stack;

public class PushCommand implements Command {
    @Override
    public void getName() {
        System.out.println(this.getClass());
    }

    @Override
    public void doCommand(Map<String, Double> context, Stack<Double> stack, String[] param) {

        if (param.length != 1) throw new NumberFormatException();

        Double d = context.get(param[0]);
        if (d == null) {
            d = Double.valueOf(param[0]);
        }
        stack.push(d);
    }
}
