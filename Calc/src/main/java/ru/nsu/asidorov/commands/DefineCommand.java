package ru.nsu.asidorov.commands;

import ru.nsu.asidorov.Command;

import java.util.Map;
import java.util.Stack;

public class DefineCommand implements Command {

    //protected static Map<String, Double> defines = new HashMap<>();//todo: изолировать

    @Override
    public void getName() {
        System.out.println(this.getClass());
    }

    @Override
    public void doCommand(Map<String, Double> context, Stack<Double> stack, String[] param) throws NumberFormatException {
        if (param.length != 2) throw new NumberFormatException();
        context.put(param[0], Double.valueOf(param[1]));
    }
}
