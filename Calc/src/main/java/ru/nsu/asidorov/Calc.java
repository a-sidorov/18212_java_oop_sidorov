package ru.nsu.asidorov;

import ch.qos.logback.classic.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.*;


public class Calc {
    private static final Logger log = (Logger) LoggerFactory.getLogger(Calc.class);

    private static Map<String, Double> context = new HashMap<>();

    public static void main(String[] args) {

        Factory factory = new Factory();
        Stack<Double> stack = new Stack<>();

        Command command;

        String[] strLine;
        String s;
        try {
            if (args.length < 1) throw new FileNotFoundException();

            log.info("Reading from file");
            FileInputStream fstream = new FileInputStream(args[0]);
            log.info("File was found");
            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

            while ((s = br.readLine()) != null) {
                strLine = s.split(" ", 2);
                command = factory.getInstance(strLine[0]);
                try {
                    if (command == null) throw new CommandException("Error: wrong command");

                    log.info("Command was found");

                    if (strLine.length > 1) {
                        command.doCommand(context, stack, strLine[1].split(" "));
                    } else {
                        command.doCommand(context, stack, null);
                    }

                } catch (CommandException e1) {
                    log.info(e1.getMessage());
                    System.out.println(e1.getMessage());
                } catch (NumberFormatException e1) {
                    log.info("Error: wrong arguement");
                    System.out.println("Error: wrong arguement");
                } catch (EmptyStackException e1) {
                    log.info("Error: empty stack");
                    System.out.println("Error: empty stack");
                }
            }
        } catch (FileNotFoundException e) {
            log.info("Reading from console");
            Scanner scanner = new Scanner(System.in);
            while (!(s = scanner.nextLine()).equals("")) {
                strLine = s.split(" ", 2);
                command = factory.getInstance(strLine[0]);
                try {
                    if (command == null) {
                        throw new CommandException("Error: wrong command");
                    }
                    log.info("Command was found");
                    if (strLine.length > 1) {
                        command.doCommand(context, stack, strLine[1].split(" "));
                    } else {
                        command.doCommand(context, stack, null);
                    }
                } catch (NumberFormatException e1) {
                    log.info("Error: wrong arguement");
                    System.out.println("Error: wrong arguement");
                } catch (CommandException | IllegalArgumentException e1) {
                    log.info(e1.getMessage());
                    System.out.println(e1.getMessage());
                } catch (EmptyStackException e1) {
                    log.info("Error: empty stack");
                    System.out.println("Error: empty stack");
                }
            }
        } catch (IOException e) {
            System.out.println("Error");
        }
        log.info("Programme is finished");
    }
}

