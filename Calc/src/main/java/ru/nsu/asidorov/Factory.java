package ru.nsu.asidorov;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class Factory {
    private static final String PATH_TO_PROPERTIES = "/factory.properties";


    private final Map<String, Class<Command>> map = new HashMap<>();

    public Factory() {
        InputStream fileInputStream = Factory.class.getResourceAsStream(PATH_TO_PROPERTIES);

        Properties prop = new Properties();
        try {
            prop.load(fileInputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (String name : prop.stringPropertyNames()) {
            try {
                // map.put(name, (Command) Class.forName(prop.getProperty(name)).getDeclaredConstructor().newInstance());
                map.put(name, (Class<Command>) Class.forName(prop.getProperty(name)));
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    Command getInstance(String command) {
        try {
            return map.getOrDefault(command, null).getDeclaredConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException | NullPointerException e) {
            e.printStackTrace();
            return null;
        }
    }
}
