package ru.nsu.asidorov.commands;

import org.junit.jupiter.api.Test;
import ru.nsu.asidorov.Command;

import java.util.EmptyStackException;
import java.util.Stack;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class DivCommandTest {

    @Test
    void doCommand() {
        Command command = new DivCommand();
        Stack<Double> stack = new Stack<>();
        stack.push(4.0);
        stack.push(2.0);
        String param = "It will be ignored";

        command.doCommand(null, stack, param.split(" "));
        assertEquals(stack.pop(), 0.5);
        assertThrows(EmptyStackException.class,stack::pop);

        stack.push(0.0);
        stack.push(4.0);

        assertThrows(IllegalArgumentException.class, () -> command.doCommand(null, stack, param.split(" ")), "Division by zero");
    }
}