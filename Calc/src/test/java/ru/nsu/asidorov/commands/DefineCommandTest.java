package ru.nsu.asidorov.commands;

import org.junit.jupiter.api.Test;
import ru.nsu.asidorov.Command;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class DefineCommandTest {

    @Test
    void doCommand() {
        Map<String, Double> context = new HashMap<>();
        Command command = new DefineCommand();
        Stack<Double> stack = new Stack<>();
        String param = "a 5";

        command.doCommand(context, stack, param.split(" ", 2));

        Command command1 = new PushCommand();
        param = "a";
        command1.doCommand(context, stack, param.split(" ", 2));

        assertEquals(stack.pop(), 5.0);

        String finalParam = "b";
        assertThrows(NumberFormatException.class, () -> command.doCommand(context, stack, finalParam.split(" ")));
    }
}