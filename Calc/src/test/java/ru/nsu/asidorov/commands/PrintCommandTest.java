package ru.nsu.asidorov.commands;


import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import ru.nsu.asidorov.Command;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Stack;

import static org.junit.jupiter.api.Assertions.assertEquals;


class PrintCommandTest {

    private static final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    private static final PrintStream originalOut = System.out;

    @BeforeAll
    static void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @AfterAll
    static void restoreStreams() {
        System.setOut(originalOut);

    }

    @Test
    void doCommand() {
        Command command = new PrintCommand();
        Stack<Double> stack = new Stack<>();
        String param = "It will be ignored";
        stack.push(4.0);

        command.doCommand(null, stack, param.split(" "));
        //assertEquals(outContent.toString(),stack.peek().toString());
        assertEquals(stack.pop(), 4.0);

    }
}