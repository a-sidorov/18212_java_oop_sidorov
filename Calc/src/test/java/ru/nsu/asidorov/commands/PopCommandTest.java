package ru.nsu.asidorov.commands;

import org.junit.jupiter.api.Test;
import ru.nsu.asidorov.Command;

import java.util.EmptyStackException;
import java.util.Stack;

import static org.junit.jupiter.api.Assertions.assertThrows;

class PopCommandTest {

    @Test
    void doCommand() {
        Command command = new PopCommand();
        Stack<Double> stack = new Stack<>();
        String param = "It will be ignored";
        stack.push(4.0);

        command.doCommand(null, stack, param.split(" "));
        assertThrows(EmptyStackException.class, stack::pop);
    }
}