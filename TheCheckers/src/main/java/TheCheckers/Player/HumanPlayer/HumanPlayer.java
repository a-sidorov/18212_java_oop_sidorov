package TheCheckers.Player.HumanPlayer;

import TheCheckers.Controller.Controller;
import TheCheckers.Model.Piece;
import TheCheckers.Player.Move;
import TheCheckers.Player.Player;
import TheCheckers.Visualisation.GUI;

public class HumanPlayer implements Player {

    private final Piece.Color color;
    private boolean ignoreClicks = true;

    private GUI view;

    public HumanPlayer(Piece.Color color) {
        this.color = color;
    }

    public void setView(GUI view) {
        this.view = view;
    }

    @Override
    public Piece.Color getColor() {
        return color;
    }

    public boolean isIgnoreClicks() {
        return ignoreClicks;
    }

    @Override
    public synchronized Move getMove(Controller controller) {
        view.updateStatus();
        ignoreClicks = false;
        try {
            this.wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ignoreClicks = true;
        return this.view.getMove();
    }
}
