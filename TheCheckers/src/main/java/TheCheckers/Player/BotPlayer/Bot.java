package TheCheckers.Player.BotPlayer;

import TheCheckers.Controller.Controller;
import TheCheckers.Model.Cell;
import TheCheckers.Model.Piece;
import TheCheckers.Player.Move;
import TheCheckers.Player.Player;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public class Bot implements Player {

    private final Piece.Color color;

    public Bot(Piece.Color color) {
        this.color = color;
    }


    @Override
    public Move getMove(Controller controller) {

        Map<Cell, Vector<Cell>> moves = new HashMap<>();
        Vector<Cell> possible = new Vector<>();

        if (controller.needKill()) {
            Boolean[] canKill = {false};
            for (int i = 0; i < 8; i++) {
                for (int j = 0; j < 8; j++) {
                    if (controller.getBoard().getCell(i, j).isOccupied() && controller.getBoard().getPiece(i, j).getColor() == color) {
                        Vector<Cell> tmp = controller.getPossibleMoves(controller.getBoard().getPiece(i, j), canKill);
                        if (canKill[0] && tmp != null && !tmp.isEmpty()) {
                            moves.put(controller.getBoard().getCell(i, j), tmp);
                            possible.add(controller.getBoard().getCell(i, j));
                        }
                    }
                }
            }
        } else {
            for (int i = 0; i < 8; i++) {
                for (int j = 0; j < 8; j++) {
                    if (controller.getBoard().getCell(i, j).isOccupied() && controller.getBoard().getPiece(i, j).getColor() == color) {
                        Vector<Cell> tmp = controller.getPossibleMoves(controller.getBoard().getPiece(i, j), null);
                        if (tmp != null && !tmp.isEmpty()) {
                            moves.put(controller.getBoard().getCell(i, j), tmp);
                            possible.add(controller.getBoard().getCell(i, j));
                        }
                    }
                }
            }
        }

        Collections.shuffle(possible);

        Cell fromCell = possible.firstElement();
        Vector<Cell> possibleMoves = moves.get(fromCell);
        Collections.shuffle(possibleMoves);
        Cell toCell = possibleMoves.firstElement();

        Move.Point from = new Move.Point(fromCell.getRow(), fromCell.getCol());
        Move.Point to = new Move.Point(toCell.getRow(), toCell.getCol());

        return new Move(from, to);
    }

    @Override
    public Piece.Color getColor() {
        return color;
    }
}
