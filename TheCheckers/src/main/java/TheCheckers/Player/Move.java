package TheCheckers.Player;

public class Move {
    final Point to;
    final Point from;

    public Move(Point from, Point to) {
        this.to = to;
        this.from = from;
    }

    public Point getTo() {
        return to;
    }

    public Point getFrom() {
        return from;
    }

    public static class Point {
        final int row;
        final int col;

        public Point(int row, int col) {
            this.row = row;
            this.col = col;
        }

        public int getRow() {
            return row;
        }

        public int getCol() {
            return col;
        }
    }
}
