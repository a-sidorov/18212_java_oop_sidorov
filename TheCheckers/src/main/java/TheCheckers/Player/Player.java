package TheCheckers.Player;

import TheCheckers.Controller.Controller;
import TheCheckers.Model.Piece;

public interface Player {
    Move getMove(Controller controller);

    Piece.Color getColor();
}
