package TheCheckers.Model;


import TheCheckers.Visualisation.Observable;
import TheCheckers.Visualisation.Observer;

import java.util.LinkedList;
import java.util.List;

public class Cell implements Observable {
    private final BackgroundColor bgColor;
    private final int row;
    private final int col;
    private final List<Observer> observers;
    private boolean isOccupied = false;

    private boolean isHighlighted = false;

    private Piece occupant;

    public Cell(BackgroundColor bgColor, int row, int col) {
        observers = new LinkedList<>();
        this.bgColor = bgColor;
        this.row = row;
        this.col = col;
    }

    @Override
    public void registerObserver(Observer o) {
        observers.add(o);
    }

    public void setHighlighted(boolean highlighted) {
        isHighlighted = highlighted;
        notifyObservers();
    }

    @Override
    public void removeObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : observers) {
            observer.update(occupant, isHighlighted);
        }
    }

    public Piece getOccupant() {
        return occupant;
    }

    public void setOccupant(Piece occupant) {

        isOccupied = occupant != null;

        this.occupant = occupant;
        notifyObservers();
    }

    public boolean isOccupied() {
        return isOccupied;
    }

    public BackgroundColor getBgColor() {
        return bgColor;
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public enum BackgroundColor {WHITE, BLACK}
}
