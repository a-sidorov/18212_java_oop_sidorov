package TheCheckers.Model;

public class Board {
    private final Cell[][] gameBoard;
    private final int rows = 8;
    private final int cols = 8;

    public Board() {
        gameBoard = new Cell[rows][cols];

        //Set up the game board with alternating colors
        boolean lastcolor = false;

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {

                if (lastcolor)
                    gameBoard[i][j] = new Cell(Cell.BackgroundColor.BLACK, i, j);
                else
                    gameBoard[i][j] = new Cell(Cell.BackgroundColor.WHITE, i, j);

                //Toggle lastcolor
                lastcolor = !lastcolor;
            }

            //Switch starting color for next row
            lastcolor = !lastcolor;
        }


    }

    public boolean inBounds(int row, int col) {
        return row >= 0 && row < rows && col >= 0 && col < cols;
    }

    public Cell getCell(int row, int col) {
        if (inBounds(row, col)) {
            return gameBoard[row][col];
        }
        return null;
    }

    public void setPiece(Piece p, int row, int col) {
        gameBoard[row][col].setOccupant(p);
    }

    public Piece getPiece(int row, int col) {
        if (this.inBounds(row, col)) {
            return this.getCell(row, col).getOccupant();
        }
        return null;
    }

    public void placeStartingPieces() {

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                setPiece(null, i, j);
            }
        }

        //Put the White side on top, Black side on bottom
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 8; col++) {
                if (getCell(row, col).getBgColor() == Cell.BackgroundColor.BLACK) {
                    setPiece(new Piece(Piece.Color.WHITE, row, col), row, col);
                }
            }
        }

        for (int row = 5; row < 8; row++) {
            for (int col = 0; col < 8; col++) {
                if (getCell(row, col).getBgColor() == Cell.BackgroundColor.BLACK) {
                    setPiece(new Piece(Piece.Color.BLACK, row, col), row, col);
                }
            }
        }
    }

    public int getRows() {
        return rows;
    }

    public int getCols() {
        return cols;
    }
}
