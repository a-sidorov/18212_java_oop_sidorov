package TheCheckers.Model;

public class Piece {

    private final Color color;
    private int row;
    private int col;
    private boolean isKing;

    public Piece(Color c, int row, int col) {
        color = c;
        this.row = row;
        this.col = col;
        this.isKing = false;
    }

    public int getRow() {
        return row;
    }

    public Color getColor() {
        return color;
    }

    public boolean isKing() {
        return isKing;
    }

    public void setKing(boolean king) {
        isKing = king;
    }

    public int getCol() {
        return col;
    }

    public void setLoc(int row, int col) {
        this.row = row;
        this.col = col;
    }

    public enum Color {WHITE, BLACK}
}
