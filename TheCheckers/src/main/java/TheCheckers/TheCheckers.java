package TheCheckers;

import TheCheckers.Controller.Controller;
import TheCheckers.Model.Piece;
import TheCheckers.Player.BotPlayer.Bot;
import TheCheckers.Player.HumanPlayer.HumanPlayer;
import TheCheckers.Player.Player;
import TheCheckers.Visualisation.GUI;

public class TheCheckers {
    public static void main(String[] args) {
        boolean again;

        HumanPlayer p1 = new HumanPlayer(Piece.Color.BLACK);
        Player p2 = new Bot(Piece.Color.WHITE);

        Controller controller = new Controller(p1, p2);

        GUI gui = new GUI(controller, p1);

        do {
            controller.run();
            again = gui.notifyWinner(controller.isWinner());
        } while (again);
    }
}
