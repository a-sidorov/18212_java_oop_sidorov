package TheCheckers.Controller;

import TheCheckers.Model.Board;
import TheCheckers.Model.Cell;
import TheCheckers.Model.Piece;
import TheCheckers.Player.Player;

import java.util.Vector;


public class Controller implements Runnable {
    private final Board board;
    Player p1;
    Player p2;
    /**
     * false - BLACK , true - White
     **/
    private boolean winner;
    private int blackCheckersLeft;
    private int whiteCheckersLeft;
    private Piece.Color currentTurn;

    public Controller(Player player1, Player player2) {
        this.board = new Board();
        this.currentTurn = Piece.Color.BLACK;
        this.blackCheckersLeft = 12;
        this.whiteCheckersLeft = 12;

        if (player1.getColor() == player2.getColor()) {
            throw new IllegalArgumentException();
        }

        if (player1.getColor() == Piece.Color.BLACK) {
            this.p1 = player1;
            this.p2 = player2;
        } else {
            this.p1 = player2;
            this.p2 = player1;
        }


    }

    public int getBlackCheckersLeft() {
        return blackCheckersLeft;
    }

    public int getWhiteCheckersLeft() {
        return whiteCheckersLeft;
    }

    public Piece.Color getCurrentTurn() {
        return currentTurn;
    }

    public Board getBoard() {
        return board;
    }

    public String getStatus() {
        return "Turn: " + getCurrentTurn().toString() + " White left: " + getWhiteCheckersLeft() + " Black left: " + getBlackCheckersLeft();
    }

    @Override
    public void run() {
        board.placeStartingPieces();
        whiteCheckersLeft = 12;
        blackCheckersLeft = 12;

        currentTurn = Piece.Color.BLACK;

        Player currentPlayer = p1;

        do {
            var move = currentPlayer.getMove(this);

            Cell from = this.board.getCell(move.getFrom().getRow(), move.getFrom().getCol());
            Cell to = this.board.getCell(move.getTo().getRow(), move.getTo().getCol());

            boolean isKilled = this.move(from, to);
            if (isKilled) {
                if (currentTurn == Piece.Color.BLACK) {
                    whiteCheckersLeft--;
                } else if (currentTurn == Piece.Color.WHITE) {
                    blackCheckersLeft--;
                }
            }

            if (currentTurn == Piece.Color.BLACK) {
                currentPlayer = p2;
                currentTurn = Piece.Color.WHITE;
            } else {
                currentPlayer = p1;
                currentTurn = Piece.Color.BLACK;
            }

        } while (whiteCheckersLeft > 0 && blackCheckersLeft > 0);

        if (whiteCheckersLeft == 0) {
            winner = false;
            return;
        }
        if (blackCheckersLeft == 0) {
            winner = true;
        }
    }

    public boolean isWinner() {
        return winner;
    }

    private boolean killProcessing(int rowFrom, int rowTo, int colFrom, int colTo) {
        for (int i = rowFrom + 1; i < rowTo; i++) {
            for (int j = colFrom + 1; j < colTo; j++) {
                if (board.getCell(i, j).isOccupied()) {
                    board.setPiece(null, i, j);
                    return true;
                }

            }
        }
        return false;
    }


    public Vector<Cell> getPossibleMoves(Piece p, Boolean[] canKill) {
        Vector<Cell> possibleMoves = new Vector<>();
        Vector<Cell> toKill = new Vector<>();

        Piece.Color pColor = p.getColor();

        int row = p.getRow();
        int col = p.getCol();

        if (canKill != null) canKill[0] = false;

        //Check moves to the top-left of this piece
        for (int i = 1; i < 8; i++) {
            if (board.inBounds(row - i, col - i)) {
                if (!board.getCell(row - i, col - i).isOccupied() && (pColor == Piece.Color.BLACK || p.isKing())) {
                    possibleMoves.add(board.getCell(row - i, col - i));
                } else if (board.inBounds(row - (i + 1), col - (i + 1)) && board.getCell(row - i, col - i).isOccupied()) {
                    if (!board.getCell(row - (i + 1), col - (i + 1)).isOccupied() &&
                            (board.getCell(row - i, col - i).getOccupant().getColor() != pColor)) {
                        toKill.add(board.getCell(row - (i + 1), col - (i + 1)));
                        if (board.getCell(row, col).getOccupant().isKing()) {
                            //for king
                            for (int j = i + 2; j < 8; j++) {
                                if (board.inBounds(row - j, col - j)) {
                                    if (!board.getCell(row - j, col - j).isOccupied()) {
                                        toKill.add(board.getCell(row - j, col - j));
                                    } else break;
                                } else break;
                            }

                        }
                        if (canKill != null) canKill[0] = true;
                    } else break;
                }
            }
            if (!p.isKing()) break;
        }


        //Check moves to the top-right of this piece
        for (int i = 1; i < 8; i++) {
            if (board.inBounds(row - i, col + i)) {
                if (!board.getCell(row - i, col + i).isOccupied() && (pColor == Piece.Color.BLACK || p.isKing())) {
                    possibleMoves.add(board.getCell(row - i, col + i));
                } else if (board.inBounds(row - (i + 1), col + (i + 1)) && board.getCell(row - i, col + i).isOccupied()) {
                    if (!board.getCell(row - (i + 1), col + (i + 1)).isOccupied() &&
                            (board.getCell(row - i, col + i).getOccupant().getColor() != pColor)) {
                        toKill.add(board.getCell(row - (i + 1), col + (i + 1)));
                        if (board.getCell(row, col).getOccupant().isKing()) {
                            //for king
                            for (int j = i + 2; j < 8; j++) {
                                if (board.inBounds(row - j, col + j)) {
                                    if (!board.getCell(row - j, col + j).isOccupied()) {
                                        toKill.add(board.getCell(row - j, col + j));
                                    } else break;
                                } else break;
                            }

                        }
                        if (canKill != null) canKill[0] = true;
                    } else break;
                }
            }
            if (!p.isKing()) break;
        }


        //check moves to the bottom-left of this piece
        for (int i = 1; i < 8; i++) {
            if (board.inBounds(row + i, col - i)) {

                if (!board.getCell(row + i, col - i).isOccupied() && (pColor == Piece.Color.WHITE || p.isKing())) {
                    possibleMoves.add(board.getCell(row + i, col - i));
                } else if (board.inBounds(row + (i + 1), col - (i + 1)) && board.getCell(row + i, col - i).isOccupied()) {

                    if (!board.getCell(row + (i + 1), col - (i + 1)).isOccupied() &&
                            (board.getCell(row + i, col - i).getOccupant().getColor() != pColor)) {
                        toKill.add(board.getCell(row + (i + 1), col - (i + 1)));
                        if (board.getCell(row, col).getOccupant().isKing()) {
                            //for king
                            for (int j = i + 2; j < 8; j++) {
                                if (board.inBounds(row + j, col - j)) {
                                    if (!board.getCell(row + j, col - j).isOccupied()) {
                                        toKill.add(board.getCell(row + j, col - j));
                                    } else break;
                                } else break;
                            }

                        }
                        if (canKill != null) canKill[0] = true;
                    } else break;
                }
            }
            if (!p.isKing()) break;
        }


        //check moves to the bottom-right of this piece
        for (int i = 1; i < 8; i++) {
            if (board.inBounds(row + i, col + i)) {

                if (!board.getCell(row + i, col + i).isOccupied() && (pColor == Piece.Color.WHITE || p.isKing()))
                    possibleMoves.add(board.getCell(row + i, col + i));

                else if (board.inBounds(row + (i + 1), col + (i + 1)) && board.getCell(row + i, col + i).isOccupied()) {

                    if (!board.getCell(row + (i + 1), col + (i + 1)).isOccupied() &&
                            (board.getCell(row + i, col + i).getOccupant().getColor() != pColor)) {
                        toKill.add(board.getCell(row + (i + 1), col + (i + 1)));
                        if (board.getCell(row, col).getOccupant().isKing()) {
                            //for king
                            for (int j = i + 2; j < 8; j++) {
                                if (board.inBounds(row + j, col + j)) {
                                    if (!board.getCell(row + j, col + j).isOccupied()) {
                                        toKill.add(board.getCell(row + j, col + j));
                                    } else break;
                                } else break;
                            }

                        }
                        if (canKill != null) canKill[0] = true;
                    } else break;
                }
            }
            if (!p.isKing()) break;
        }


        if (toKill.isEmpty()) {
            return possibleMoves;
        } else return toKill;

    }

    public Boolean needKill() {
        Boolean[] cankill = {false};
        for (int i = 0; i < board.getRows(); i++) {
            for (int j = 0; j < board.getCols(); j++) {
                Cell cell = board.getCell(i, j);
                if (cell.isOccupied() && cell.getOccupant().getColor() == currentTurn) {
                    getPossibleMoves(cell.getOccupant(), cankill);
                }
                if (cankill[0]) break;
            }
            if (cankill[0]) break;
        }
        return cankill[0];
    }

    public boolean move(Cell from, Cell to) {
        boolean isKilled;

        Piece beingMoved = from.getOccupant();

        int oldRow = from.getRow();
        int newRow = to.getRow();
        int oldCol = from.getCol();
        int newCol = to.getCol();

        if ((beingMoved.getColor() == Piece.Color.BLACK && newRow == 0) || (beingMoved.getColor() == Piece.Color.WHITE && newRow == 7)) {
            beingMoved.setKing(true);
        }

        board.setPiece(null, oldRow, oldCol);
        beingMoved.setLoc(newRow, newCol);
        board.setPiece(beingMoved, newRow, newCol);

        if (newRow > oldRow) {
            if (newCol > oldCol) {
                isKilled = this.killProcessing(oldRow, newRow, oldCol, newCol);
            } else {
                isKilled = this.killProcessing(oldRow, newRow, newCol, oldCol);
            }
        } else {
            if (newCol > oldCol) {
                isKilled = this.killProcessing(newRow, oldRow, oldCol, newCol);
            } else {
                isKilled = this.killProcessing(newRow, oldRow, newCol, oldCol);
            }
        }

        return isKilled;
    }
}
