package TheCheckers.Visualisation;

import TheCheckers.Controller.Controller;
import TheCheckers.Model.Board;
import TheCheckers.Model.Cell;
import TheCheckers.Player.HumanPlayer.HumanPlayer;
import TheCheckers.Player.Move;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Vector;


public class GUI extends MouseAdapter {
    private final JFrame frame;

    private final JLabel piecesLabel;


    private final HumanPlayer player;
    private final Controller controller;
    private Move.Point from;
    private Move.Point to;
    private Vector<Cell> validMoves;

    public GUI(Controller controller, HumanPlayer player) {
        this.controller = controller;

        this.player = player;
        this.player.setView(this);

        frame = new JFrame("The Russian Checkers");

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new FlowLayout());

        frame.getContentPane().setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));

        piecesLabel = new JLabel(" ");
        piecesLabel.setHorizontalTextPosition(JLabel.LEFT);
        piecesLabel.setVerticalTextPosition(JLabel.BOTTOM);


        JPanel boardpanel = new JPanel(new GridLayout(8, 8));
        boardpanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        addObservers(this.controller.getBoard(), boardpanel);

        frame.add(piecesLabel);
        frame.add(boardpanel);

        frame.pack();


        Rectangle boundingRect = frame.getBounds();
        frame.setBounds(boundingRect.x, boundingRect.y, boundingRect.width + 30, boundingRect.height + 30);

        frame.setVisible(true);
        updateStatus();
    }

    public void updateStatus() {
        piecesLabel.setText(controller.getStatus());
    }

    public boolean notifyWinner(boolean winner) {
        String winnerMessage;
        if (winner) {
            winnerMessage = "Winner is White";
        } else {
            winnerMessage = "Winner is Black";
        }

        int YesNo = JOptionPane.showConfirmDialog(this.frame, winnerMessage, "Do you want play again?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (YesNo == 0) {
            return true;
        } else {
            frame.dispose();
            return false;
        }
    }

    private void addObservers(Board b, JPanel p) {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                CellObserver o = new CellObserver(b.getCell(i, j).getBgColor(), i, j);
                b.getCell(i, j).registerObserver(o);
                o.addMouseListener(this);

                JPanel ContainerPanel = new JPanel(new FlowLayout());
                ContainerPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
                ContainerPanel.add(o);
                if (o.getColor() == Cell.BackgroundColor.BLACK) {
                    ContainerPanel.setBackground(Color.DARK_GRAY);
                } else {
                    ContainerPanel.setBackground(Color.LIGHT_GRAY);
                }
                p.add(ContainerPanel);
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {

        boolean needKill = controller.needKill();

        updateStatus();

        if (this.player.isIgnoreClicks()) {
            piecesLabel.setText("It is not your turn!");
            return;
        }

        CellObserver sel = (CellObserver) mouseEvent.getComponent();
        Boolean[] canKill = {false};

        if (from == null) {

            if (!controller.getBoard().getCell(sel.getRow(), sel.getCol()).isOccupied()) {
                piecesLabel.setText("You can`t do this!");
                return;
            }

            if (controller.getBoard().getCell(sel.getRow(), sel.getCol()).getOccupant().getColor() != player.getColor()) {
                piecesLabel.setText("It is not your Piece!");
                return;
            }


            validMoves = controller.getPossibleMoves(controller.getBoard().getPiece(sel.getRow(), sel.getCol()), canKill);
            if (needKill && !canKill[0]) {
                piecesLabel.setText("You may kill enemy!");
                return;
            }

            for (Cell move : validMoves) {
                move.setHighlighted(true);
            }

            from = new Move.Point(sel.getRow(), sel.getCol());
        } else {
            if (sel.getRow() == from.getRow() && sel.getCol() == from.getCol()) {
                for (Cell move : validMoves) {
                    move.setHighlighted(false);
                }
                from = null;
                return;
            }

            if (!validMoves.contains(controller.getBoard().getCell(sel.getRow(), sel.getCol()))) {
                for (Cell move : validMoves) {
                    move.setHighlighted(false);
                }
                validMoves = controller.getPossibleMoves(controller.getBoard().getPiece(sel.getRow(), sel.getCol()), canKill);
                for (Cell move : validMoves) {
                    move.setHighlighted(true);
                }
                from = new Move.Point(sel.getRow(), sel.getCol());
                return;
            }

            for (Cell move : validMoves) {
                move.setHighlighted(false);
            }

            to = new Move.Point(sel.getRow(), sel.getCol());
            updateStatus();

            synchronized (this.player) {
                this.player.notify();
            }
        }
    }

    public Move getMove() {
        Move move = new Move(from, to);
        from = null;
        to = null;

        return move;
    }
}
