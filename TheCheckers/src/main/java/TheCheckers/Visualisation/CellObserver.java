package TheCheckers.Visualisation;


import TheCheckers.Model.Cell;
import TheCheckers.Model.Piece;

import javax.swing.*;
import java.awt.*;

public class CellObserver extends Canvas implements Observer {
    private static final Image black;
    private static final Image white;
    private static final Image bking;
    private static final Image wking;
    private static final Image possible;

    static {
        try {
            black = new ImageIcon(CellObserver.class.getResource("/BPawn.png")).getImage();
            white = new ImageIcon(CellObserver.class.getResource("/WPawn.png")).getImage();
            bking = new ImageIcon(CellObserver.class.getResource("/BKing.png")).getImage();
            wking = new ImageIcon(CellObserver.class.getResource("/WKing.png")).getImage();
            possible = new ImageIcon(CellObserver.class.getResource("/possible.png")).getImage();
        } catch (NullPointerException e) {
            e.printStackTrace();
            throw e;
        }

    }

    final Cell.BackgroundColor color;
    private final int row;
    private final int col;
    Piece piece;
    boolean isOccupied;
    private boolean isHighlighted;

    public CellObserver(Cell.BackgroundColor bgColor, int row, int col) {
        this.setSize(64, 64);
        this.color = bgColor;
        this.row = row;
        this.col = col;
        this.piece = null;
        this.isOccupied = false;


    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public Cell.BackgroundColor getColor() {
        return color;
    }

    @Override
    public void update(Piece p, boolean isHighlighted) {
        this.isOccupied = p != null;
        this.piece = p;
        this.isHighlighted = isHighlighted;
        this.update(this.getGraphics());
    }


    @Override
    public void paint(Graphics g) {
        if (this.color == Cell.BackgroundColor.BLACK) {
            this.setBackground(Color.DARK_GRAY);
        } else {
            this.setBackground(Color.LIGHT_GRAY);
        }


        if (this.isOccupied) {
            if (!piece.isKing()) {
                if (piece.getColor() == Piece.Color.WHITE) {
                    g.drawImage(white, 0, 0, 64, 64, null);
                }

                if (piece.getColor() == Piece.Color.BLACK) {
                    g.drawImage(black, 0, 0, 64, 64, null);
                }
            } else {
                if (piece.getColor() == Piece.Color.WHITE) {
                    g.drawImage(wking, 0, 0, 64, 64, null);
                }

                if (piece.getColor() == Piece.Color.BLACK) {
                    g.drawImage(bking, 0, 0, 64, 64, null);
                }
            }

        } else if (this.isHighlighted) {
            g.drawImage(possible, 0, 0, 64, 64, null);
        } else {
            g.clearRect(0, 0, 64, 64);
        }
    }
}
