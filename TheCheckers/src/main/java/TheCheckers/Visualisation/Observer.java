package TheCheckers.Visualisation;

import TheCheckers.Model.Piece;

public interface Observer {
    void update(Piece p, boolean isHighlighted);
}
