package server;

import model.Piece;
import utils.boadcontroller.BoardController;
import utils.connection.Connection;
import utils.connection.ConnectionListener;
import utils.connection.exception.ProtocolViolationException;
import utils.connection.protocol.Protocol;
import utils.data.Move;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import static utils.connection.protocol.Protocol.*;


public class Server extends Thread {


    private final ServerSocket serverSocket;

    private final BoardController controller;

    private Piece.Color currentTurn;
    private Connection player1;
    private Connection player2;
    private Thread p1Listener;
    private Thread p2Listener;

    private boolean lastkill = false;

    public Server(int port) throws IOException {
        this.serverSocket = new ServerSocket(port, 2);

        System.out.println(this.serverSocket.getInetAddress());

        this.controller = new BoardController();
        this.player1 = null;
        this.player2 = null;
        this.currentTurn = Piece.Color.BLACK;
    }

    private void waitPlayers() throws IOException {
        this.player1 = null;
        this.player2 = null;

        while (true) {
            Socket socket;

            socket = this.serverSocket.accept();

            System.out.println("Connection...");

            Connection tmp = new Connection(socket);

            byte[] data = tmp.receive();

            if (data[0] != BLACK && data[0] != WHITE) {
                tmp.send(new byte[]{DENIED});
                System.out.println("Client denied");
                continue;
            }

            if (data[0] == BLACK) {

                if (this.player1 != null) {
                    tmp.send(new byte[]{BUSY_COLOR});

                    System.out.println("Black is busy. Denied");

                    continue;
                }

                this.player1 = tmp;

                tmp.send(new byte[]{Protocol.ACCEPTED});

                System.out.println("BLACK connected");

                if (this.player2 != null && !this.player2.isClosed()) {
                    System.out.println("Players connected");
                    break;
                }

                continue;
            }

            if (data[0] == Protocol.WHITE) {
                if (this.player2 != null) {
                    tmp.send(new byte[]{Protocol.BUSY_COLOR});

                    System.out.println("White is busy. Denied");

                    continue;
                }

                this.player2 = tmp;

                tmp.send(new byte[]{ACCEPTED});

                System.out.println("WHITE connected");

                if (this.player1 != null && !this.player1.isClosed()) {
                    System.out.println("Players connected");
                    break;
                }
            }


        }

    }

    @Override
    public void run() {
        this.controller.placeStartingPieces();
        this.currentTurn = Piece.Color.BLACK;

        System.out.println("Waiting players.....");

        try {
            this.waitPlayers();
        } catch (IOException e) {
            System.out.println("Waiting players err");
            e.printStackTrace();
            return;
        }

        System.out.println("Game started");
        p1Listener = new Thread(new ClientConnectionListener(player1));
        p2Listener = new Thread(new ClientConnectionListener(player2));

        p1Listener.start();
        p2Listener.start();

        try {
            player1.send(new byte[]{START});
            player2.send(new byte[]{START});
        } catch (IOException ignored) {
        }


        Connection currentPlayer = player1;
        synchronized (this) {
            while (this.controller.getWhiteCheckersLeft() != 0 && this.controller.getBlackCheckersLeft() != 0) {
                try {
                    currentPlayer.send(new byte[]{MOVE});
                } catch (IOException e) {
                    this.shutdown();
                    return;
                }

                try {
                    this.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    this.shutdown();
                    return;
                }

                if (currentTurn == Piece.Color.BLACK && !(lastkill && controller.needKill(currentTurn))) {
                    currentPlayer = player2;
                    currentTurn = Piece.Color.WHITE;
                    continue;
                }

                if (currentTurn == Piece.Color.WHITE && !(lastkill && controller.needKill(currentTurn))) {
                    currentPlayer = player1;
                    currentTurn = Piece.Color.BLACK;
                }
            }
        }

        if (this.controller.getWhiteCheckersLeft() == 0) {
            try {
                player1.send(new byte[]{WIN});
                player2.send(new byte[]{LOSE});
            } catch (IOException e) {
                e.printStackTrace();
                this.shutdown();
                return;
            }

        }

        if (this.controller.getBlackCheckersLeft() == 0) {
            try {
                player1.send(new byte[]{LOSE});
                player2.send(new byte[]{WIN});
            } catch (IOException e) {
                e.printStackTrace();
                this.shutdown();
                return;
            }
        }

        this.shutdown();
    }

    private synchronized void shutdown() {
        this.p1Listener.interrupt();
        this.p2Listener.interrupt();

        try {
            player1.close();
            player2.close();
        } catch (IOException ignored) {
        }
        this.notify();
        this.interrupt();
        try {
            this.join();
        } catch (InterruptedException ignored) {
        }
    }


    private final class ClientConnectionListener extends ConnectionListener {

        protected ClientConnectionListener(Connection connection) {
            super(connection);
        }

        @Override
        public void onDataReceived(byte[] data) throws IOException {
            synchronized (Server.this) {
                if (data.length < 1 || data.length > 5) throw new ProtocolViolationException("Bad data");

                if (data.length == 1) {
                    if (data[0] == REQUEST) {
                        super.connection.send(controller.getBoard().serializeBoard());
                    } else {
                        throw new ProtocolViolationException("Bad data");
                    }
                }

                if (data.length == 5) {
                    if (data[0] == MOVE) {
                        byte[] moveBytes = new byte[4];
                        System.arraycopy(data, 1, moveBytes, 0, 4);

                        Move move = Move.deserializeMove(moveBytes);
                        if (!controller.validateMove(move, currentTurn)) {
                            connection.send(new byte[]{ERR_MOVE});
                        } else {

                            connection.send(new byte[]{ACCEPTED});

                            Move.Point tmp = move.getTo();
                            Boolean[] canKill = {false};

                            boolean kill = controller.move(move);
                            controller.getPossibleMoves(controller.getBoard().getPiece(tmp.getRow(), tmp.getCol()), canKill);
                            lastkill = kill && canKill[0];

                            Server.this.notify();
                        }
                    } else throw new ProtocolViolationException("Bad data");
                }
            }

        }

        @Override
        public void onConnectionClosed() {
            Server.this.shutdown();
        }

        @Override
        public void onIOException(IOException e) {
            onConnectionClosed();
        }

        @Override
        public void onProtocolViolationException(ProtocolViolationException e) throws IOException {
            connection.send(new byte[]{REQUEST});
        }
    }
}
