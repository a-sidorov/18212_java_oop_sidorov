package server;

import java.io.IOException;

public class ServerApplication {
    public static void main(String[] args) throws IOException {
        Server server = new Server(6868);
        server.start();
    }
}
