package model;

public class Piece {

    private final Color color;
    private byte row;
    private byte col;
    private boolean isKing;

    public Piece(Color c, byte row, byte col) {
        color = c;
        this.row = row;
        this.col = col;
        this.isKing = false;
    }

    public byte getRow() {
        return row;
    }

    public byte getCol() {
        return col;
    }

    public Color getColor() {
        return color;
    }

    public boolean isKing() {
        return isKing;
    }

    public void setKing(boolean king) {
        isKing = king;
    }


    public void setLoc(byte row, byte col) {
        this.row = row;
        this.col = col;
    }

    public enum Color {WHITE, BLACK}
}
