package model;

import java.io.ByteArrayOutputStream;

public class Board {
    private static final byte EMPTY = 0;
    private static final byte BLACK = 1;
    private static final byte WHITE = 2;
    private static final byte B_KING = 3;
    private static final byte W_KING = 4;
    private final Cell[][] gameBoard;
    private final byte rows = 8;
    private final byte cols = 8;


    public int getBlackCheckersLeft() {
        int blackCheckersLeft = 0;

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (gameBoard[i][j].isOccupied()) {
                    if (gameBoard[i][j].getOccupant().getColor() == Piece.Color.BLACK)
                        blackCheckersLeft++;
                }
            }
        }

        return blackCheckersLeft;
    }

    public int getWhiteCheckersLeft() {

        int whiteCheckersLeft = 0;

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (gameBoard[i][j].isOccupied()) {
                    if (gameBoard[i][j].getOccupant().getColor() == Piece.Color.WHITE)
                        whiteCheckersLeft++;
                }
            }
        }

        return whiteCheckersLeft;
    }

    public Board() {
        gameBoard = new Cell[rows][cols];

        //Set up the game board with alternating colors
        boolean lastcolor = false;

        for (byte i = 0; i < rows; i++) {
            for (byte j = 0; j < cols; j++) {

                if (lastcolor)
                    gameBoard[i][j] = new Cell(Cell.BackgroundColor.BLACK, i, j);
                else
                    gameBoard[i][j] = new Cell(Cell.BackgroundColor.WHITE, i, j);

                //Toggle lastcolor
                lastcolor = !lastcolor;
            }

            //Switch starting color for next row
            lastcolor = !lastcolor;
        }


    }

    public boolean inBounds(int row, int col) {
        return row >= 0 && row < rows && col >= 0 && col < cols;
    }

    public Cell getCell(int row, int col) {
        if (inBounds(row, col)) {
            return gameBoard[row][col];
        }
        return null;
    }

    public void setPiece(Piece p, int row, int col) {
        gameBoard[row][col].setOccupant(p);
    }

    public Piece getPiece(byte row, byte col) {
        if (this.inBounds(row, col)) {
            return this.getCell(row, col).getOccupant();
        }
        return null;
    }

    public void placeStartingPieces() {

        for (byte i = 0; i < 8; i++) {
            for (byte j = 0; j < 8; j++) {
                setPiece(null, i, j);
            }
        }

        //Put the White side on top, Black side on bottom
        for (byte row = 0; row < 3; row++) {
            for (byte col = 0; col < 8; col++) {
                if (getCell(row, col).getBgColor() == Cell.BackgroundColor.BLACK) {
                    setPiece(new Piece(Piece.Color.WHITE, row, col), row, col);
                }
            }
        }

        for (byte row = 5; row < 8; row++) {
            for (byte col = 0; col < 8; col++) {
                if (getCell(row, col).getBgColor() == Cell.BackgroundColor.BLACK) {
                    setPiece(new Piece(Piece.Color.BLACK, row, col), row, col);
                }
            }
        }
    }

    public byte[] serializeBoard() {
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();

        for (int i = 0; i < this.rows; i++) {
            for (int j = 0; j < this.cols; j++) {
                if (!this.getCell(i, j).isOccupied()) {
                    byteStream.write(EMPTY);
                    continue;
                }
                if (this.getCell(i, j).getOccupant().getColor() == Piece.Color.BLACK) {
                    if (this.getCell(i, j).getOccupant().isKing()) byteStream.write(B_KING);
                    else byteStream.write(BLACK);
                }
                if (this.getCell(i, j).getOccupant().getColor() == Piece.Color.WHITE) {
                    if (this.getCell(i, j).getOccupant().isKing()) byteStream.write(W_KING);
                    else byteStream.write(WHITE);
                }
            }
        }

        return byteStream.toByteArray();
    }

    public void deserializeBoard(byte[] data) {
        for (byte i = 0; i < this.rows; i++) {
            for (byte j = 0; j < this.cols; j++) {
                byte state = data[this.rows * i + j];
                if (state == EMPTY) this.setPiece(null, i, j);
                if (state == WHITE) this.setPiece(new Piece(Piece.Color.WHITE, i, j), i, j);
                if (state == BLACK) this.setPiece(new Piece(Piece.Color.BLACK, i, j), i, j);
                if (state == W_KING) {
                    Piece p = new Piece(Piece.Color.WHITE, i, j);
                    p.setKing(true);
                    this.setPiece(p, i, j);
                }
                if (state == B_KING) {
                    Piece p = new Piece(Piece.Color.BLACK, i, j);
                    p.setKing(true);
                    this.setPiece(p, i, j);
                }
            }
        }
    }

    public int getRows() {
        return rows;
    }

    public int getCols() {
        return cols;
    }
}
