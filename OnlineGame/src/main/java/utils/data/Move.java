package utils.data;

public class Move {
    final Point to;
    final Point from;

    public Move(Point from, Point to) {
        this.to = to;
        this.from = from;
    }

    public static Move deserializeMove(byte[] data) {
        return new Move(new Point(data[0], data[1]), new Point(data[2], data[3]));
    }

    public Point getTo() {
        return to;
    }

    public Point getFrom() {
        return from;
    }

    public byte[] serializeMove() {
        byte[] result = new byte[4];
        result[0] = from.row;
        result[1] = from.col;
        result[2] = to.row;
        result[3] = to.col;
        return result;
    }

    public static class Point {
        final byte row;
        final byte col;

        public Point(byte row, byte col) {
            this.row = row;
            this.col = col;
        }

        public byte getRow() {
            return row;
        }

        public byte getCol() {
            return col;
        }
    }
}
