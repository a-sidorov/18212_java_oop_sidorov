package utils.connection;

import utils.connection.exception.ProtocolViolationException;

import java.io.Closeable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

public class Connection implements Closeable {
    private final Socket socket;

    public Connection(Socket socket) {
        this.socket = socket;
    }

    public Connection(InetAddress address, int port) throws IOException {
        this(new Socket(address, port));
    }

    public void send(byte[] data) throws IOException {
        this.sendBytes(data);
    }


    public byte[] receive() throws IOException {
        DataInputStream dataInputStream = new DataInputStream(this.socket.getInputStream());
        int messageLength = dataInputStream.readInt();

        byte[] data = new byte[messageLength];
        if (dataInputStream.read(data, 0, messageLength) != messageLength) {
            throw new ProtocolViolationException("Data lost");
        }

        // return dataInputStream.readNBytes(messageLength);
        return data;
    }

    private void sendBytes(byte[] data) throws IOException {
        DataOutputStream dataOutputStream = new DataOutputStream(this.socket.getOutputStream());
        dataOutputStream.writeInt(data.length);
        dataOutputStream.write(data);
        dataOutputStream.flush();
    }

    public InetAddress getInetAddress() {
        return this.socket.getInetAddress();
    }

    public int getPort() {
        return this.socket.getPort();
    }

    public boolean isClosed() {
        return this.socket.isClosed();
    }

    @Override
    public String toString() {
        return String.format("%s:%d", this.getInetAddress(), this.getPort());
    }

    @Override
    public void close() throws IOException {
        this.socket.close();
    }
}