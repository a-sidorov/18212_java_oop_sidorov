package utils.connection.protocol;

public abstract class Protocol {
    public static final byte BLACK = 0;
    public static final byte WHITE = 1;
    public static final byte ACCEPTED = 2;
    public static final byte BUSY_COLOR = 3;
    public static final byte DENIED = 4;
    public static final byte WIN = 5;
    public static final byte LOSE = 6;

    public static final byte REQUEST = 7;
    public static final byte MOVE = 8;
    public static final byte ERR_MOVE = 9;
    public static final byte START = 10;
}
