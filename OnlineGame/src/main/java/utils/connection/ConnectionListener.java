package utils.connection;

import utils.connection.exception.ProtocolViolationException;

import java.io.EOFException;
import java.io.IOException;

public abstract class ConnectionListener implements Runnable {
    protected final Connection connection;

    protected ConnectionListener(Connection connection) {
        this.connection = connection;
    }


    @Override
    public final void run() {
        while (true) {

            if (connection.isClosed()) {
                onConnectionClosed();
            }

            try {
                byte[] input = this.connection.receive();

                this.onDataReceived(input);
            } catch (ProtocolViolationException e) {
                try {
                    this.onProtocolViolationException(e);
                } catch (IOException ignored) {
                }
                break;
            } catch (IOException e) {
                if (!this.connection.isClosed() && !(e instanceof EOFException)) {
                    this.onIOException(e);
                }
                try {
                    this.connection.close();
                } catch (IOException ignored) {
                }
                break;
            }
        }
    }

    public abstract void onDataReceived(byte[] data) throws IOException;

    public abstract void onConnectionClosed();

    public abstract void onIOException(IOException e);

    public abstract void onProtocolViolationException(ProtocolViolationException e) throws IOException;
}