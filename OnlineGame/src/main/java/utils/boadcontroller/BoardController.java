package utils.boadcontroller;

import model.Board;
import model.Cell;
import model.Piece;
import utils.data.Move;

import java.util.Vector;

public class BoardController {
    private final Board board;

    public BoardController() {
        this.board = new Board();
    }

    public Vector<Cell> getPossibleMoves(Piece p, Boolean[] canKill) {
        Vector<Cell> possibleMoves = new Vector<>();
        Vector<Cell> toKill = new Vector<>();

        Piece.Color pColor = p.getColor();

        int row = p.getRow();
        int col = p.getCol();

        if (canKill != null) canKill[0] = false;

        //Check moves to the top-left of this piece
        topleft:
        for (int i = 1; i < 8; i++) {
            if (board.inBounds(row - i, col - i)) {
                if (!board.getCell(row - i, col - i).isOccupied() && (pColor == Piece.Color.BLACK || p.isKing())) {
                    possibleMoves.add(board.getCell(row - i, col - i));
                } else if (board.inBounds(row - (i + 1), col - (i + 1)) && board.getCell(row - i, col - i).isOccupied()) {
                    if (!board.getCell(row - (i + 1), col - (i + 1)).isOccupied() &&
                            (board.getCell(row - i, col - i).getOccupant().getColor() != pColor)) {
                        toKill.add(board.getCell(row - (i + 1), col - (i + 1)));
                        if (board.getCell(row, col).getOccupant().isKing()) {
                            //for king
                            for (int j = i + 2; j < 8; j++) {
                                if (board.inBounds(row - j, col - j)) {
                                    if (!board.getCell(row - j, col - j).isOccupied()) {
                                        toKill.add(board.getCell(row - j, col - j));
                                    } else break topleft;
                                } else break topleft;
                            }

                        }
                    } else break;
                }
            }
            if (!p.isKing()) break;
        }


        //Check moves to the top-right of this piece
        topright:
        for (int i = 1; i < 8; i++) {
            if (board.inBounds(row - i, col + i)) {
                if (!board.getCell(row - i, col + i).isOccupied() && (pColor == Piece.Color.BLACK || p.isKing())) {
                    possibleMoves.add(board.getCell(row - i, col + i));
                } else if (board.inBounds(row - (i + 1), col + (i + 1)) && board.getCell(row - i, col + i).isOccupied()) {
                    if (!board.getCell(row - (i + 1), col + (i + 1)).isOccupied() &&
                            (board.getCell(row - i, col + i).getOccupant().getColor() != pColor)) {
                        toKill.add(board.getCell(row - (i + 1), col + (i + 1)));
                        if (board.getCell(row, col).getOccupant().isKing()) {
                            //for king
                            for (int j = i + 2; j < 8; j++) {
                                if (board.inBounds(row - j, col + j)) {
                                    if (!board.getCell(row - j, col + j).isOccupied()) {
                                        toKill.add(board.getCell(row - j, col + j));
                                    } else break topright;
                                } else break topright;
                            }

                        }
                    } else break;
                }
            }
            if (!p.isKing()) break;
        }


        //check moves to the bottom-left of this piece
        botleft:
        for (int i = 1; i < 8; i++) {
            if (board.inBounds(row + i, col - i)) {

                if (!board.getCell(row + i, col - i).isOccupied() && (pColor == Piece.Color.WHITE || p.isKing())) {
                    possibleMoves.add(board.getCell(row + i, col - i));
                } else if (board.inBounds(row + (i + 1), col - (i + 1)) && board.getCell(row + i, col - i).isOccupied()) {

                    if (!board.getCell(row + (i + 1), col - (i + 1)).isOccupied() &&
                            (board.getCell(row + i, col - i).getOccupant().getColor() != pColor)) {
                        toKill.add(board.getCell(row + (i + 1), col - (i + 1)));
                        if (board.getCell(row, col).getOccupant().isKing()) {
                            //for king
                            for (int j = i + 2; j < 8; j++) {
                                if (board.inBounds(row + j, col - j)) {
                                    if (!board.getCell(row + j, col - j).isOccupied()) {
                                        toKill.add(board.getCell(row + j, col - j));
                                    } else break botleft;
                                } else break botleft;
                            }

                        }
                    } else break;
                }
            }
            if (!p.isKing()) break;
        }


        //check moves to the bottom-right of this piece
        botright:
        for (int i = 1; i < 8; i++) {
            if (board.inBounds(row + i, col + i)) {

                if (!board.getCell(row + i, col + i).isOccupied() && (pColor == Piece.Color.WHITE || p.isKing()))
                    possibleMoves.add(board.getCell(row + i, col + i));

                else if (board.inBounds(row + (i + 1), col + (i + 1)) && board.getCell(row + i, col + i).isOccupied()) {

                    if (!board.getCell(row + (i + 1), col + (i + 1)).isOccupied() &&
                            (board.getCell(row + i, col + i).getOccupant().getColor() != pColor)) {
                        toKill.add(board.getCell(row + (i + 1), col + (i + 1)));
                        if (board.getCell(row, col).getOccupant().isKing()) {
                            //for king
                            for (int j = i + 2; j < 8; j++) {
                                if (board.inBounds(row + j, col + j)) {
                                    if (!board.getCell(row + j, col + j).isOccupied()) {
                                        toKill.add(board.getCell(row + j, col + j));
                                    } else break botright;
                                } else break botright;
                            }

                        }
                    } else break;
                }
            }
            if (!p.isKing()) break;
        }


        if (toKill.isEmpty()) {
            return possibleMoves;
        } else {
            if (canKill != null) canKill[0] = true;
            return toKill;
        }

    }

    public Board getBoard() {
        return board;
    }

    public int getBlackCheckersLeft() {
        return board.getBlackCheckersLeft();
    }

    public int getWhiteCheckersLeft() {
        return board.getWhiteCheckersLeft();
    }

    public void placeStartingPieces() {
        this.board.placeStartingPieces();
    }


    public Boolean needKill(Piece.Color currentTurn) {
        Boolean[] cankill = {false};
        for (int i = 0; i < board.getRows(); i++) {
            for (int j = 0; j < board.getCols(); j++) {
                Cell cell = board.getCell(i, j);
                if (cell.isOccupied() && cell.getOccupant().getColor() == currentTurn) {
                    getPossibleMoves(cell.getOccupant(), cankill);
                }
                if (cankill[0]) break;
            }
            if (cankill[0]) break;
        }
        return cankill[0];
    }

    private boolean move(Cell from, Cell to) {
        boolean isKilled;

        Piece beingMoved = from.getOccupant();

        byte oldRow = from.getRow();
        byte newRow = to.getRow();
        byte oldCol = from.getCol();
        byte newCol = to.getCol();

        if ((beingMoved.getColor() == Piece.Color.BLACK && newRow == 0) || (beingMoved.getColor() == Piece.Color.WHITE && newRow == 7)) {
            beingMoved.setKing(true);
        }

        isKilled = false;

        if (newRow > oldRow) {
            if (newCol > oldCol) {
                for (int i = oldRow + 1, j = oldCol + 1; i < newRow && j < newCol; i++, j++) {
                    if (board.getCell(i, j).isOccupied()) {
                        board.setPiece(null, i, j);
                        isKilled = true;
                        break;
                    }
                }
            } else if (oldCol > newCol) {
                for (int i = oldRow + 1, j = oldCol - 1; i < newRow && j > newCol; i++, j--) {
                    if (board.getCell(i, j).isOccupied()) {
                        board.setPiece(null, i, j);
                        isKilled = true;
                        break;
                    }
                }
            }
        } else if (oldRow > newRow) {
            if (newCol > oldCol) {
                for (int i = oldRow - 1, j = oldCol + 1; i > newRow && j < newCol; i--, j++) {
                    if (board.getCell(i, j).isOccupied()) {
                        board.setPiece(null, i, j);
                        isKilled = true;
                        break;
                    }
                }
            } else if (oldCol > newCol) {
                for (int i = oldRow - 1, j = oldCol - 1; i > newRow && j > newCol; i--, j--) {
                    if (board.getCell(i, j).isOccupied()) {
                        board.setPiece(null, i, j);
                        isKilled = true;
                        break;
                    }
                }
            }
        }


        board.setPiece(null, oldRow, oldCol);
        beingMoved.setLoc(newRow, newCol);
        board.setPiece(beingMoved, newRow, newCol);

        return isKilled;
    }

    public boolean move(Move move) {
        Move.Point from = move.getFrom();
        Move.Point to = move.getTo();

        Cell fromCell = board.getCell(from.getRow(), from.getCol());
        Cell toCell = board.getCell(to.getRow(), to.getCol());

        return move(fromCell, toCell);
    }

    public boolean validateMove(Move move, Piece.Color currentTurn) {
        Move.Point from = move.getFrom();
        Move.Point to = move.getTo();

        if (!(board.inBounds(from.getRow(), from.getCol()) && board.inBounds(to.getRow(), to.getCol()))) {
            return false;
        }

        if (!board.getCell(from.getRow(), from.getCol()).isOccupied()) return false;
        if (board.getCell(to.getRow(), to.getCol()).isOccupied()) return false;

        Piece p = board.getPiece(from.getRow(), from.getCol());

        if (p.getColor() != currentTurn) return false;

        Boolean[] canKill = {false};

        Vector<Cell> possible = this.getPossibleMoves(p, canKill);

        if (!possible.contains(board.getCell(to.getRow(), to.getCol()))) return false;

        Boolean needKill = this.needKill(currentTurn);

        return needKill == canKill[0];
    }
}
