package utils;


import model.Piece;

public interface Observer {
    void update(Piece p, boolean isHighlighted);
}
