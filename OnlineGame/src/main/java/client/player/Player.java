package client.player;

import model.Piece;
import utils.boadcontroller.BoardController;
import utils.data.Move;

public interface Player {
    Move getMove(BoardController controller);

    Piece.Color getColor();
}
