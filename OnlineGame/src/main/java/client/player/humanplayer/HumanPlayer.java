package client.player.humanplayer;

import client.player.Player;
import client.visualisation.GUI;
import model.Piece;
import utils.boadcontroller.BoardController;
import utils.data.Move;

public class HumanPlayer implements Player {
    private GUI view;
    private boolean ignoreClicks = true;
    private Piece.Color color = Piece.Color.BLACK;


    @Override
    public synchronized Move getMove(BoardController controller) {
        //view.updateStatus();
        ignoreClicks = false;
        try {
            this.wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ignoreClicks = true;
        return this.view.getMove();
    }

    public void changeColor() {
        if (this.color == Piece.Color.BLACK) {
            this.color = Piece.Color.WHITE;
        } else this.color = Piece.Color.BLACK;
    }

    public boolean isIgnoreClicks() {
        return ignoreClicks;
    }

    @Override
    public Piece.Color getColor() {
        return color;
    }

    public void setView(GUI view) {
        this.view = view;
    }
}
