package client;

import client.player.humanplayer.HumanPlayer;
import client.visualisation.GUI;
import model.Piece;
import utils.boadcontroller.BoardController;
import utils.connection.Connection;
import utils.connection.exception.ProtocolViolationException;
import utils.data.Move;

import java.io.IOException;
import java.net.InetAddress;

import static utils.connection.protocol.Protocol.*;

public class Client implements Runnable {
    private final BoardController controller;
    private final GUI gui;
    private final HumanPlayer player;
    private Connection server;

    public Client() throws IOException {
        controller = new BoardController();
        player = new HumanPlayer();
        gui = new GUI(controller, player);

        try {
            connect();
        } catch (IOException e) {
            gui.showMessage("Connection failed: " + e.getMessage());
            gui.close();
            throw e;
        }

    }

    private void shutdown() {
        try {
            server.close();
        } catch (IOException ignored) {
        }
    }

    private void connect() throws IOException {
        Connection tmp;

        InetAddress address = InetAddress.getByName(gui.getIp());

        while (true) {
            tmp = new Connection(address, 6868);
            if (player.getColor() == Piece.Color.BLACK)
                tmp.send(new byte[]{BLACK});
            else tmp.send(new byte[]{WHITE});

            byte[] status = tmp.receive();

            if (status[0] != ACCEPTED && status[0] != BUSY_COLOR) {
                throw new ProtocolViolationException("Bad status");
            }

            if (status[0] == ACCEPTED) break;

            player.changeColor();
        }

        server = tmp;
        gui.showMessage("Please wait opponent");
    }

    @Override
    public void run() {

        byte[] status;

        status = getStatus();
        if (status == null) return;

        if (status[0] != START) {
            gui.showMessage("Protocol err");
            gui.close();
            shutdown();
            return;
        }

        gui.showMessage("Opponent connected");
        controller.placeStartingPieces();
        gui.updateStatus();

        while (true) {

            if (server.isClosed()) {
                gui.showMessage("Connection lost");
                gui.close();
                shutdown();
                return;
            }

            status = getStatus();
            if (status == null) return;

            if (status[0] == WIN || status[0] == LOSE) {
                gui.notifyWinner(status[0] == WIN);
                shutdown();
            } else if (status[0] != MOVE) {
                gui.showMessage("Protocol err");
                gui.close();
                shutdown();
                return;
            }

            try {
                server.send(new byte[]{REQUEST});
                byte[] boardData = server.receive();
                controller.getBoard().deserializeBoard(boardData);
            } catch (IOException e) {
                gui.showMessage("Connection lost");
                gui.close();
                shutdown();
                return;
            }

            gui.updateStatus();

            do {
                Move move = player.getMove(controller);
                byte[] result = move.serializeMove();
                byte[] answer = new byte[5];
                answer[0] = MOVE;
                System.arraycopy(result, 0, answer, 1, 4);
                try {
                    server.send(answer);
                    status = server.receive();
                } catch (IOException e) {
                    gui.showMessage("Connection lost");
                    gui.close();
                    shutdown();
                    return;
                }

                if (status.length != 1) {
                    gui.showMessage("Protocol err");
                    gui.close();
                    shutdown();
                    return;
                }

                if (status[0] == ACCEPTED) {
                    controller.move(move);
                    gui.updateStatus();
                }

            } while (status[0] != ACCEPTED);

        }
    }

    private byte[] getStatus() {
        byte[] status;
        try {
            status = server.receive();
        } catch (IOException e) {
            gui.showMessage("Connection lost");
            gui.close();
            shutdown();
            return null;
        }

        if (status.length != 1) {
            gui.showMessage("Protocol err");
            gui.close();
            shutdown();
            return null;
        }
        return status;
    }
}
