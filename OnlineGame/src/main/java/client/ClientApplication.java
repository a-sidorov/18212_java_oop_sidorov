package client;


public class ClientApplication {

    public static void main(String[] args) {
        Client client;

        try {
            client = new Client();
        } catch (Exception e) {
            return;
        }
        client.run();
    }

}
