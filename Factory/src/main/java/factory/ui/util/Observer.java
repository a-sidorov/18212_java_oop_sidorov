package factory.ui.util;

public interface Observer {

    void update();
}
