package factory.ui.util;

public interface Observable {

    void setObserver(Observer observer);
}
