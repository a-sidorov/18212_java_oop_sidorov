package factory.ui.gui.tab;

import factory.factory.Factory;
import factory.factory.car.parts.Accessory;
import factory.factory.dealer.Dealer;
import factory.factory.supplier.PartsSupplier;
import factory.ui.gui.util.PeriodicDelaySliderPanel;
import factory.ui.gui.util.VerticalScrollablePanel;

import javax.swing.*;
import java.util.ArrayList;


public class SettingsTab extends JTabbedPane {
    private static final String SUPPLIERS_SETTINGS_TAB = "Suppliers Settings";
    private static final String DEALERS_SETTINGS_TAB = "Dealers Settings";

    public SettingsTab(Factory.Suppliers suppliers, Factory.Dealers dealers) {
        this.init(suppliers, dealers);
    }

    private void init(Factory.Suppliers suppliers, Factory.Dealers dealers) {
        ArrayList<JComponent> suppliersSlidersList = new ArrayList<>(suppliers.accessoriesSuppliers.size() + 2);

        suppliersSlidersList.add(new PeriodicDelaySliderPanel("Bodies Supplier", suppliers.bodiesSupplier));
        suppliersSlidersList.add(new PeriodicDelaySliderPanel("Motors Supplier", suppliers.motorsSupplier));

        for (PartsSupplier<Accessory> accessoriesSupplier : suppliers.accessoriesSuppliers) {
            suppliersSlidersList.add(new PeriodicDelaySliderPanel("Accessories Supplier No" + (accessoriesSupplier.getNumber()), accessoriesSupplier));
        }

        this.addTab(SUPPLIERS_SETTINGS_TAB, new VerticalScrollablePanel(suppliersSlidersList));

        ArrayList<JComponent> dealersSlidersList = new ArrayList<>(dealers.dealers.size());

        for (Dealer dealer : dealers.dealers) {
            dealersSlidersList.add(new PeriodicDelaySliderPanel(dealer.toString(), dealer));
        }

        this.addTab(DEALERS_SETTINGS_TAB, new VerticalScrollablePanel(dealersSlidersList));
    }
}
