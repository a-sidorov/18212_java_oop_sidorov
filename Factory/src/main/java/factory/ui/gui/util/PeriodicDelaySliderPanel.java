package factory.ui.gui.util;

import factory.util.Periodic;

import javax.swing.*;
import java.awt.*;

public final class PeriodicDelaySliderPanel extends JPanel {
    private static final int MIN_PERIOD_SECOND = 1;
    private static final int MAX_PERIOD_SECOND = 60;

    private int lastValue;

    public PeriodicDelaySliderPanel(String name, Periodic periodic) {
        this.init(name, periodic);
    }

    private void init(String name, Periodic periodic) {
        this.setLayout(new BorderLayout());
        JSlider slider = new JSlider(SwingConstants.HORIZONTAL, MIN_PERIOD_SECOND, MAX_PERIOD_SECOND, periodic.getDelayMillis() / 1000);

        this.lastValue = periodic.getDelayMillis() / 1000;

        JLabel current = new JLabel(String.format("Delay of %s is %d s", name, this.lastValue));

        this.add(current, BorderLayout.NORTH);

        slider.addChangeListener(changeEvent -> {
            JSlider source = (JSlider) changeEvent.getSource();

            if (!source.getValueIsAdjusting()) {
                PeriodicDelaySliderPanel.this.lastValue = source.getValue();

                periodic.setDelayMillis(PeriodicDelaySliderPanel.this.lastValue * 1000);

                current.setText(String.format("Delay of %s is %d s", name, PeriodicDelaySliderPanel.this.lastValue));
            } else {
                current.setText(String.format("Delay of %s will be %d s (%d s now)", name, source.getValue(), PeriodicDelaySliderPanel.this.lastValue));
            }

        });
        slider.setMajorTickSpacing(1);
        slider.setPaintTicks(true);

        this.add(slider, BorderLayout.CENTER);
    }
}
