package factory.factory.storage;

import factory.factory.Factory;
import factory.factory.car.Car;
import factory.factory.car.parts.Accessory;
import factory.factory.car.parts.Body;
import factory.factory.car.parts.Motor;
import factory.ui.util.Observable;
import factory.ui.util.Observer;
import factory.util.Task;
import factory.util.UniqueObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public final class CarStorageController extends UniqueObject implements Observable {
    private static final Logger logger = LoggerFactory.getLogger(CarStorageController.class.getSimpleName());

    private final Factory factory;

    private int carsDispatched = 0;

    private Observer observer;

    public CarStorageController(Factory factory) {
        this.factory = factory;
    }

    public Car requestNewCar() throws InterruptedException {
        logger.debug(this.toString() + " received a request for a new car");

        synchronized (this.factory.getStorageArea().carsStorage) {
            if (this.factory.getStorageArea().carsStorage.isEmpty()) {
                int upperBound = this.factory.getStorageArea().getMaximumPossibleCarsCount() - this.factory.getWorkersPool().getQueuedTasksCount();
                if (upperBound <= 0) {
                    upperBound = 1;
                }

                logger.debug(this.toString() + " dispatches " + upperBound + " car assembly tasks (" + this.factory.getWorkersPool().getQueuedTasksCount() + " queued)");
                for (int i = 0; i < upperBound; ++i) {
                    this.factory.getWorkersPool().execute(new CarAssemblyTask(this.factory.getStorageArea()));
                }
            }

            while (this.factory.getStorageArea().carsStorage.isEmpty()) {
                logger.debug(this.toString() + " waits for a new car to be assembled");
                this.factory.getStorageArea().carsStorage.wait();
            }

            Car newCar = this.factory.getStorageArea().carsStorage.get();

            logger.debug(this.toString() + " returns " + newCar);

            this.factory.getStorageArea().carsStorage.notify();

            this.carsDispatched += 1;

            if (this.observer != null) {
                this.observer.update();
            }

            return newCar;
        }
    }

    public int getDispatchedCarsCount() {
        return this.carsDispatched;
    }

    @Override
    public void setObserver(Observer observer) {
        this.observer = observer;
    }

    public static final class CarAssemblyTask extends UniqueObject implements Task {

        private static final Logger logger = LoggerFactory.getLogger(CarAssemblyTask.class.getSimpleName());

        private static final int ASSEMBLING_TIME_MILLIS = 5000;

        private final Factory.StorageArea storageArea;

        CarAssemblyTask(Factory.StorageArea storageArea) {
            this.storageArea = storageArea;
        }

        private Body getBody() throws InterruptedException {

            synchronized (this.storageArea.bodiesStorage) {
                while (this.storageArea.bodiesStorage.isEmpty()) {
                    this.storageArea.bodiesStorage.wait();
                }

                Body body = this.storageArea.bodiesStorage.get();

                logger.debug(this.toString() + " has taken " + body + " from " + this.storageArea.bodiesStorage);

                this.storageArea.bodiesStorage.notify();
                return body;
            }
        }

        private Motor getMotor() throws InterruptedException {

            synchronized (this.storageArea.motorsStorage) {
                while (this.storageArea.motorsStorage.isEmpty()) {
                    this.storageArea.motorsStorage.wait();
                }

                Motor motor = this.storageArea.motorsStorage.get();
                logger.debug(this.toString() + " has taken " + motor + " from " + this.storageArea.motorsStorage);
                this.storageArea.motorsStorage.notify();
                return motor;
            }
        }

        private Accessory getAccessory() throws InterruptedException {
            synchronized (this.storageArea.accessoriesStorage) {
                while (this.storageArea.accessoriesStorage.isEmpty()) {
                    this.storageArea.accessoriesStorage.wait();
                }

                Accessory accessory = this.storageArea.accessoriesStorage.get();
                logger.debug(this.toString() + " has taken " + accessory + " from " + this.storageArea.accessoriesStorage);
                this.storageArea.accessoriesStorage.notify();
                return accessory;
            }
        }

        private void dispatchCar(Car newCar) throws InterruptedException {
            synchronized (this.storageArea.carsStorage) {
                while (this.storageArea.carsStorage.isFull()) {
                    this.storageArea.carsStorage.wait();
                }

                this.storageArea.carsStorage.add(newCar);

                logger.debug(this.toString() + " dispatched " + newCar + " to " + this.storageArea.carsStorage);
                this.storageArea.carsStorage.notify();
            }
        }

        @Override
        public void assemble() {
            try {
                final Body body = this.getBody();
                final Motor motor = this.getMotor();
                final Accessory accessory = this.getAccessory();

                Thread.sleep(ASSEMBLING_TIME_MILLIS);

                this.dispatchCar(new Car(body, motor, accessory));
            } catch (InterruptedException e) {
                logger.warn(this.toString() + " was interrupted");
            }
        }
    }
}
