package factory.factory.dealer;


import factory.factory.car.Car;
import factory.factory.storage.CarStorageController;
import factory.util.Periodic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.atomic.AtomicInteger;

public class Dealer extends Thread implements Periodic {
    private final int number;
    private static final Logger logger = LoggerFactory.getLogger(Dealer.class.getSimpleName());

    private final CarStorageController carStorageController;
    private final AtomicInteger delayMillis;

    public Dealer(int number, CarStorageController carStorageController, int delayMillis) {
        this.number = number;
        this.carStorageController = carStorageController;
        this.delayMillis = new AtomicInteger(delayMillis);
    }

    @Override
    public int getDelayMillis() {
        return this.delayMillis.get();
    }

    @Override
    public void setDelayMillis(int newDelayMillis) {
        this.delayMillis.set(newDelayMillis);
        this.interrupt();
    }

    @Override
    public String toString() {
        return String.format("Dealer %d", this.number);
    }

    @Override
    public void run() {
        while (true) {
            try {

                if (this.isInterrupted()) this.start();

                logger.debug(this.toString() + " is requesting a new car from " + this.carStorageController.toString());

                Car newCar = this.carStorageController.requestNewCar();

                logger.debug(this.toString() + " got " + newCar + " form " + this.carStorageController.toString());

                logger.info(this.toString() + ": " + newCar);
            } catch (InterruptedException e) {
                logger.warn(this.toString() + " was interrupted when attempting to get a new car");
                break;
            }

            try {
                sleep(this.delayMillis.get());
            } catch (InterruptedException e) {
                logger.warn(this.toString() + " was interrupted while sleeping");
                break;
            }
        }
    }
}
