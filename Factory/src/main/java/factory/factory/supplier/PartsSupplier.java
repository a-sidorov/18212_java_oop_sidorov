package factory.factory.supplier;

import factory.factory.storage.Storage;
import factory.util.Periodic;
import factory.util.UniqueObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;


public class PartsSupplier<T extends UniqueObject> extends Thread implements Periodic {
    private static final Logger logger = LoggerFactory.getLogger(PartsSupplier.class.getSimpleName());

    private final int number;

    private final Supplier<T> supplier;
    private final Storage<T> storage;
    private final AtomicInteger delayMillis;

    public PartsSupplier(Supplier<T> supplier, Storage<T> storage, int delayMillis, int number) throws IllegalArgumentException {
        this.number = number;

        if (delayMillis < 1) {
            throw new IllegalArgumentException("Delay must be positive");
        }

        this.supplier = supplier;
        this.storage = storage;
        this.delayMillis = new AtomicInteger(delayMillis);
    }

    @Override
    public int getDelayMillis() {
        return this.delayMillis.get();
    }

    @Override
    public void setDelayMillis(int newDelayMillis) {
        this.delayMillis.set(newDelayMillis);
        this.interrupt();
    }

    @Override
    public String toString() {
        return String.format("%s: %d", this.getClass().getSimpleName(), this.number);
    }

    public int getNumber() {
        return number;
    }

    @Override
    public void run() {
        outer:
        while (true) {
            if (this.isInterrupted()) this.start();

            T newPart = this.supplier.get();

            synchronized (this.storage) {
                while (this.storage.isFull()) {
                    try {
                        logger.debug(this.toString() + " is waiting for " + this.storage + " to become available");
                        this.storage.wait();
                    } catch (InterruptedException e) {

                        logger.warn(this.toString() + " is interrupted when waiting for " + this.storage + " to become available");
                        break outer;
                    }
                }

                this.storage.add(newPart);
                logger.debug(this.toString() + " added " + newPart + " to " + this.storage);
                this.storage.notify();
            }


            try {
                Thread.sleep(this.delayMillis.get());
            } catch (InterruptedException e) {
                logger.warn(this.toString() + " was interrupted while sleeping", this);
                break;
            }
        }
    }
}
