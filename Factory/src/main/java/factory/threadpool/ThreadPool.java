package factory.threadpool;

import factory.ui.util.Observable;
import factory.ui.util.Observer;
import factory.util.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;


public class ThreadPool implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger(ThreadPool.class.getSimpleName());

    private final Queue<Task> tasksQueue = new ConcurrentLinkedQueue<>();
    private final ArrayList<Worker> workersList;
    private volatile boolean isRunning = false;

    public ThreadPool(int threadsCount) {
        if (threadsCount < 1) {
            throw new IllegalArgumentException("Threads count must be positive");
        }

        this.workersList = new ArrayList<>(threadsCount);

        for (int i = 0; i < threadsCount; ++i) {
            Worker worker = new Worker(i + 1);
            this.workersList.add(worker);
        }
    }

    public int getQueuedTasksCount() {
        synchronized (this.tasksQueue) {
            return this.tasksQueue.size();
        }
    }

    public ArrayList<Worker> getWorkersList() {
        return this.workersList;
    }

    public void stopAcceptingTasks() {
        this.isRunning = false;
    }

    public void shutdown() {
        if (!this.isRunning) {
            return;
        }

        this.isRunning = false;

        for (Thread t : this.workersList) {
            t.interrupt();
        }

        for (Thread t : this.workersList) {
            try {
                t.join();
            } catch (InterruptedException ignored) {
            }
        }
    }


    public void execute(Task task)
            throws IllegalStateException {
        if (!this.isRunning) {
            throw new IllegalStateException("Thread pool is shut down");
        }

        synchronized (this.tasksQueue) {
            this.tasksQueue.add(task);
            this.tasksQueue.notify();
        }
    }

    @Override
    public void run() {
        if (this.isRunning) {
            return;
        }

        this.isRunning = true;

        for (Thread t : this.workersList) {
            t.start();
        }
    }

    public final class Worker extends Thread implements Observable {
        private final int number;
        private Observer observer;
        private String status;

        private Worker(int number) {
            this.number = number;
        }

        public String getStatus() {
            return this.status;
        }

        @Override
        public void run() {
            outer:
            while (ThreadPool.this.isRunning) {

                if (this.isInterrupted()) this.start();

                final Task task;
                synchronized (ThreadPool.this.tasksQueue) {
                    while (ThreadPool.this.tasksQueue.isEmpty()) {
                        try {
                            this.status = "Waiting for tasks";
                            if (this.observer != null) {
                                this.observer.update();
                            }
                            ThreadPool.this.tasksQueue.wait();
                        } catch (InterruptedException e) {
                            ThreadPool.logger.warn(ThreadPool.class.getSimpleName() + "." + this.toString() + " was interrupted while waiting for tasks");
                            break outer;
                        }
                    }
                    task = ThreadPool.this.tasksQueue.poll();
                }

                this.status = "Running task";
                if (this.observer != null) {
                    this.observer.update();
                }

                task.assemble();

                this.status = "Finished task";
                if (this.observer != null) {
                    this.observer.update();
                }
            }
        }

        @Override
        public String toString() {
            return String.format("%s: %d", this.getClass().getSimpleName(), this.number);
        }

        @Override
        public void setObserver(Observer observer) {
            this.observer = observer;
        }
    }
}
