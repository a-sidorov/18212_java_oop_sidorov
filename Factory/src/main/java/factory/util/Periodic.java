package factory.util;

public interface Periodic {

    int getDelayMillis();

    void setDelayMillis(int newDelayMillis);
}
