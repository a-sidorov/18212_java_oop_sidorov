package factory.util;

public interface Task {
    void assemble();
}
