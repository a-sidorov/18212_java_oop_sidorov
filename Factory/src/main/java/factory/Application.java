package factory;

import factory.factory.Factory;
import factory.ui.gui.MainWindow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;


public class Application {
    private static final Logger logger = LoggerFactory.getLogger(Application.class.getSimpleName());

    public static void main(String[] args) {
        final Factory factory;

        try {
            factory = new Factory();
            logger.info("Created a factory instance");
        } catch (IOException e) {
            logger.error("Failed to create a factory instance: " + e.getMessage());
            return;
        }

        logger.info("Creating main window");

        MainWindow mainWindow = new MainWindow(factory);

        logger.info("Running main window");
        mainWindow.run();
    }

}
